NOTE: The following application was only set up to work with GMail accounts

To run the following application,

1) Download email_client.jar from https://bitbucket.org/fmontei/email-client/downloads

2) Double-click on the runnable .jar file to run the application

3) Once the application is open, for information about the application, go to Help > About

4) Note: do not retrieve mail from a heavily populated inbox -- 
the application will in effect freeze, as the processing time will be very long