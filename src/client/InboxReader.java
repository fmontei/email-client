package client;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;
import javax.swing.SwingUtilities;

import org.jsoup.Jsoup;

public class InboxReader {

	private Vector <iMessage>  newMessages = new Vector <> ();
	private HashSet <String> oldMessages = new HashSet <> ();
	protected static int messageCount = 1;

	public Vector <iMessage> retrieveMail() {
		
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps"); 
		
		try {
			Session session = Session.getInstance(props, null);
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", LoginScreen.username, LoginScreen.password);
	
			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_ONLY);
			Message messages[] = inbox.getMessages();
			messageCount = inbox.getMessageCount();
			for (Message message : messages) {
				
				oldMessages.add(message.getSentDate().toString());
				
				Address[] senders = message.getFrom();
				String allSenders = "";
				for (Address s : senders) {
					
					allSenders += s.toString();
				}
				
				Object contentObject = message.getContent();
				String bodyText = "";
	            if (contentObject instanceof Multipart)
	            {
	                Multipart content = (Multipart) contentObject;
	                int count = content.getCount();
	                BodyPart clearTextPart = null, htmlTextPart = null;
	                
	                for(int i=0; i < count; i++)
	                {
	                    BodyPart part = content.getBodyPart(i);
	                    if(part.isMimeType("text/plain")) {
	                        clearTextPart = part;
	                        break;
	                    }
	                    else if(part.isMimeType("text/html"))
	                    {
	                        htmlTextPart = part;
	                        break;
	                    }
	                }
	                
	                if (clearTextPart != null) {
	                	bodyText = clearTextPart.getContent().toString();
	                }
	                
	                else if (htmlTextPart != null) {
	                    String html = (String) htmlTextPart.getContent();
	                    bodyText = Jsoup.parse(html).text();
	                }
	            }
				
				iMessage current = new iMessage(
						allSenders, 
						message.getSubject(), 
						bodyText,
						message.getReceivedDate().toString());
				
				newMessages.add(current);
			}
		} 
		
		catch (NoSuchProviderException e) {
		} catch (MessagingException e) {
		} catch (IOException e) {
		}
		
		return newMessages;
	}
	
	public Vector <iMessage> refreshMail() {
		
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps"); 
		
		try {
			Session session = Session.getInstance(props, null);
			Store store = session.getStore("imaps");	
			store.connect("imap.gmail.com", LoginScreen.username, LoginScreen.password);
		
			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_ONLY);
			Message messages[] = inbox.getMessages(messageCount, inbox.getMessageCount());
			messageCount = inbox.getMessageCount();		
			
			for (Message message : messages) {
				
				if (!oldMessages.add(message.getSentDate().toString())) continue;
				
				Address[] senders = message.getFrom();
				String allSenders = "";
				for (Address s : senders) {
					
					allSenders += s.toString();
				}
				
				iMessage current = new iMessage(
						allSenders, 
						message.getSubject(), 
						((MimeMessage) message).getContent().toString(),
						message.getReceivedDate().toString());
				
				newMessages.add(current);
			}	
			
		} 
		
		catch (NoSuchProviderException e) {
		} catch (MessagingException e) {
		} catch (IOException e) {
		}
		
		return newMessages;
	}	
}



