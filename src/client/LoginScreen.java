package client;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import java.util.Vector;

import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class LoginScreen {
	
	private JFrame frame;
	protected static String username = new String("null");
	protected static String password = new String("null");
	private JComboBox <String> contactBox = new JComboBox <> ();
	private Object[] accounts = new Object[DataStore.config.size()];
	private JOptionPane optionPane = new JOptionPane();
	private JButton connect = new JButton("Connect");
	private JButton cancel  = new JButton("Cancel");
	private Object[] params = {contactBox, connect, cancel}; 
	private static Transport transport;
	protected static Session loginSession;
	
	LoginScreen(JFrame frame) {
		
		this.frame = frame;
	}
	
	public void closeDialog() {
		
		// Close the window
		Window w = SwingUtilities.getWindowAncestor(connect);
	    if (w != null) 
	    	w.setVisible(false);
	}

	public void initLoginScreen() {
		
		loadContacts();
	    
	    connect.addActionListener(new ActionListener() {
	    	
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		if (contactBox.getSelectedIndex() == -1) {
	    			
	    			closeDialog();
	    			return;
	    		}
	    			
	    		Configuration current = (Configuration) accounts[contactBox.getSelectedIndex()];
	    		username = current.getEmailAddress();
	    		password = current.getPassword();
	    		
	    		closeDialog();
	    	}
	    });
	    
	    cancel.addActionListener(new ActionListener() {
	    	
	    	public void actionPerformed(ActionEvent e) {
	    	
	    		Window w = SwingUtilities.getWindowAncestor(cancel);
	    	    if (w != null) 
	    	    	w.setVisible(false);
	    	}
	    });
	
    	optionPane.showOptionDialog(
	    		frame, 
	    		"Select email account:\n\n", 
	    		"Email Client Login", 
	    		1, 
	    		JOptionPane.INFORMATION_MESSAGE, 
	    		null, 
	    		params, 
	    		null);
	}
	
	public void loadContacts() {
		
		try {
			
			for (int i = 0; i < DataStore.config.size(); ++i) {
				
				Configuration current = DataStore.config.get(i);
				accounts[i] = current;
				contactBox.addItem(current.getEmailAddress());
			}
		} catch(Exception e) {}
	}
	
	private static Session createSmtpSession() {
		
	    final Properties props = new Properties();
	    props.setProperty("mail.transport.protocol", "smtp");
	    props.setProperty("mail.smtp.host", "smtp.gmail.com");
	    props.setProperty("mail.smtp.auth", "true");
	    props.setProperty("mail.smtp.port", "587");
	    props.setProperty("mail.smtp.starttls.enable", "true");
	   
	    return Session.getInstance(props, new Authenticator() {
	
	        protected PasswordAuthentication getPasswordAuthentication() {

	            return new PasswordAuthentication(username, password);
	        }
	    });
	}
	
	protected static boolean validateCredentials() {
		
	    try {
	    	LoginScreen.loginSession = createSmtpSession();
	        transport = LoginScreen.loginSession.getTransport();
	        transport.connect();
	        transport.close();
	    } catch (AuthenticationFailedException e) {
	        return false;
	    } catch (MessagingException e) {
	    	return false;
	    }
	    
	    return true;
	}
}
