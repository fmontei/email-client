package client;

import java.io.Serializable;

public class Configuration implements Serializable {

	private static final long serialVersionUID = 1L;
	private String emailAddress;
	private String password;
	
	public Configuration(String email, String password) {
		
		this.emailAddress = email;
		this.password = password;
	}
	
	public void setEmailAddress(String email) {
		
		this.emailAddress = email;
	}
	
	public void setPassword(String password) {
		
		this.password = password;
	}
	
	public String getEmailAddress() {
		
		return emailAddress;
	}
	
	public String getPassword() {
		
		return password;
	}
}
