package client;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public abstract class ContactButton {

	private JFrame frame; 
	private JTextField[] textFields;
	
	ContactButton(ContactTable table) {

	}
	
	public void init() {
		
		initializeFrame();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setSize(450, 180);
		frame.setLocationRelativeTo(null);
		
		textFields = configureTextFields();
		JPanel mainPanel = new JPanel(new BorderLayout());
		JPanel subPanel = new JPanel();
		subPanel.setLayout(new GridLayout(textFields.length, 2));
		
		JLabel name = new JLabel("Name");
		name.setHorizontalAlignment(SwingConstants.CENTER);
		subPanel.add(name);
		subPanel.add(textFields[0]);
		JLabel pAddress = new JLabel("Email Address");
		pAddress.setHorizontalAlignment(SwingConstants.CENTER);
		subPanel.add(pAddress);
		subPanel.add(textFields[1]);
		JLabel eAddress = new JLabel("Physical Address");
		eAddress.setHorizontalAlignment(SwingConstants.CENTER);
		subPanel.add(eAddress);
		subPanel.add(textFields[2]);
		JLabel phone = new JLabel("Telephone Number");
		phone.setHorizontalAlignment(SwingConstants.CENTER);
		subPanel.add(phone);
		subPanel.add(textFields[3]);
		GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.weighty = 1;
		mainPanel.add(subPanel, BorderLayout.CENTER);
		JPanel buttons = new JPanel(new GridLayout(0, 2));
		
		JButton apply  = configureApplyButton();
		JButton cancel = configureCancelButton();
		buttons.add(apply);
		buttons.add(cancel);
		mainPanel.add(buttons, BorderLayout.SOUTH);
		
		frame.add(mainPanel);
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	public JFrame getFrame() {
		
		return frame;
	}
	
	public void setFrame(JFrame f) {
		
		frame = f;
	}
	
	public abstract JTextField[] configureTextFields();
	public abstract void         initializeFrame();
	public abstract JButton      configureApplyButton();
	public abstract JButton      configureCancelButton();
	public abstract void         addListeners();
}
