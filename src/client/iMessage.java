package client;

public class iMessage {

	String from, subject, content, date;
	
	public iMessage(String from, String subject, String content, String date) {
		
		this.from = from;
		this.subject = subject;
		this.content = content;
		this.date = date;
	}
	
	public String getContent() {
		
		return content;
	}
	
	public String getColumn(int col) {
		
		switch(col) {
		
			case 0:
				return from;
			case 1: 
				return subject;
			case 2: 
				return content;
			case 3:
				return date;
			default:
				return "";
		}
	}

	public void editData(Object data, int col) {
		
		switch(col) {
		
			case 0:
				from = (String) data;
				break;
			case 1: 
				subject = (String) data;
				break;
			case 2: 
				content = (String) data;
				break;
			case 3:
				date = (String) data;
				break;
		}
	}
}
