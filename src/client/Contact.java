package client;

import java.awt.Color;
import java.io.Serializable;

public class Contact implements Serializable {

	private String contactName;
	private String postalAddress;
	private String emailAddress;
	private String phoneNumber;
	
	public Contact(String name, String pAddress, String eAddress, String number) {
		
		contactName = name;
		postalAddress = pAddress;
		emailAddress = eAddress;
		phoneNumber = number;
	}
	
	public String getName() {
			
		return contactName;
	}
	
	public String getPostalAddress() {
		
		return postalAddress;
	}
	
	public String getEmailAddress() {
		
		return emailAddress;
	}
	
	public String getPhoneNumber() {
		
		return phoneNumber;
	}
	
	public void setPostalAdress(String address) {
		
		postalAddress = address;
	}
	
	public void setEmailAdress(String address) {
		
		emailAddress = address; 
	}
	
	public void setPhoneNumber(String number) {
		
		phoneNumber = number;
	}
	
	public String getColumn(int col) {
		
		switch(col) {
		
			case 0:
				return contactName;
			case 1: 
				return emailAddress;
			case 2: 
				return postalAddress;
			case 3:
				return phoneNumber;
			default:
				return "";
		}
	}

	public void editData(Object data, int col) {
		
		switch(col) {
		
			case 0:
				contactName = (String) data;
				break;
			case 1: 
				emailAddress = (String) data;
				break;
			case 2: 
				postalAddress = (String) data;
				break;
			case 3:
				phoneNumber = (String) data;
				break;
		}
	}
}
