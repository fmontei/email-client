package client;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.Utilities;

public class SystemInformationDig extends JDialog {

	private static final String MESSAGE = new String(
			"Email Client written by Felipe Monteiro and " +
			"Mike Dozier.");
	
	private JTextPane textPane = new JTextPane();
	
	public SystemInformationDig() {
		
		this.setLayout(new GridBagLayout());
		this.setTitle("Email Client Information");
		this.setResizable(false);
		this.setModalityType(Dialog.ModalityType.DOCUMENT_MODAL);
		textPane.setEditable(false);
		
		initialText(MESSAGE);
		appendText("\n\nAdding and managing an email account", true);
		appendText("\nClick on Configuration followed by Configure Client." +
				" Next, click on Add Account. Fill in the appropriate fields." +
				" Afterward, click apply. To edit account information, click" +
				" on edit. Immediate changes may not be reflected in the " +
				"drag-down menu until the apply button has been clicked.", false);
		appendText(" Note: ", true);
		appendText(" this email client is only configured to work with Gmail." +
				" Other email servers have been provided for aesthetic reasons" 
				+ " alone.", false);
		appendText("\n\nAdding Contacts", true);
		appendText("\nClick the Add Contact button and fill in" +
				" appropriate fields.", false);
		appendText("\n\nComposing emails", true);
		appendText("\nRight-click on a single or multiple selected contacts and" +
				" select Compose. Note that the client ignores all invalid" +
				" emails when adding them to the To field. Alternatively," +
				" click on any contact and manually fill in the To field " +
				"with email addresses. Put a comma between each address. " +
				"Click Send to send the email. A message will indicate" +
				" whether the email was sent successfully or not.", false);
		appendText("\n\nRetrieving email from the Inbox", true);
		appendText("\nClick on the Inbox tab near the top left of the panel." +
				" Click on Retrieve Mail and wait until the table is" +
				" populated with messages. Double click on any message " +
				"to read the message in the inbox. To switch inboxes, " +
				"go to the Configuration menu, select another account, click" +
				" Connect, followed by Save. Finally, click on Retrieve Mail. " +
				" Refresh can be used to view new messages received after " +
				"initial retrieval by the client.", false);
		appendText(" Note: Connect in the Configuration menu must be clicked" +
				" in order to switch email accounts.", true);
		textPane.setCaretPosition(0);
		
		JScrollPane pane = new JScrollPane(textPane);
		pane.setPreferredSize(new Dimension(400, 200));
	
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridy = 0;
		JButton ok = new JButton("Ok");
		ok.setPreferredSize(new Dimension(50, 20));
		ok.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		
		this.add(pane, gbc);
		gbc.gridy = 1;
		this.add(ok, gbc);
		this.pack();
		this.setLocationRelativeTo(this.getParent());
		this.setVisible(true);
	}
	
	public void appendText(String text, boolean isBold) {
	    
		StyledDocument doc = textPane.getStyledDocument();
		SimpleAttributeSet keyWord = new SimpleAttributeSet();
		StyleConstants.setForeground(keyWord, Color.BLACK);
		StyleConstants.setBold(keyWord, isBold);

	    try {
			doc.insertString(doc.getLength(), text, keyWord);
		} catch (BadLocationException e) {
		}  
	}
	
	public void initialText(String text) {
		    
		StyledDocument doc = textPane.getStyledDocument();
		SimpleAttributeSet keyWord = new SimpleAttributeSet();
		StyleConstants.setForeground(keyWord, Color.BLACK);
		StyleConstants.setItalic(keyWord, true);
	    try {
			doc.insertString(doc.getLength(), text, keyWord);
		} catch (BadLocationException e) {
		}  
	}
}
