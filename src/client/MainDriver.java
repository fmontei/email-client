package client;

import java.io.IOException;

public class MainDriver {
	
	protected static DataStore storage = DataStore.getInstance();
	
	public static void main(String[] args) {
		
		MainDriver.load();
		MainFrame frame = new MainFrame("Email Client");
		frame.setVisible(true);
	}
	
	public static void load() {
		
		try {
			MainDriver.storage.retrieve();
		} catch (ClassNotFoundException ex) {
		} catch (IOException ex) {
		}
	}
	
	public static void save() {
		
		try {
			MainDriver.storage.setContacts(DataStore.contacts);
			MainDriver.storage.store();
		} catch (IOException ex) {
		}
	}
}
