package client;

import java.io.IOException;

public interface Storable {
	void store() throws IOException;
}
