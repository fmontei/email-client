package client;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.*;


public class MainFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private final String[] contactColumns = {"Name", 
										     "Email Address",
										     "Physical Address",
										     "Telephone Number"};
	private final ContactTableModel contactModel = new ContactTableModel(
			contactColumns, 
			DataStore.contacts);
	private final ContactTable contactTable = new ContactTable("Table", contactModel);
	private final JPanel contactLayout = new JPanel(new BorderLayout());
	
	private final String[] inboxColumns = {"Name", "Subject", "Message", "Date"};
	private final InboxReader inboxReader = new InboxReader();
	private Vector <iMessage> messages = new Vector <iMessage> ();
	private EmailTableModel inboxModel = new EmailTableModel(
			inboxColumns, 
			messages);
	private EmailTable inboxTable = new EmailTable("Table", inboxModel);
	private final JPanel inboxLayout = new JPanel(new BorderLayout());
	protected static JButton refresh, retrieve;
	
	protected static JButton add, edit, remove;
	
	protected static final ImageIcon ICON = new ImageIcon("icon.jpg");
	
	private final WindowListener exitListener = new WindowAdapter() {

        public void windowClosing(WindowEvent e) {
        	
            int confirm = JOptionPane.showOptionDialog(null, 
	    		"Are you sure you want to close the application?", 
	    		"Exit Confirmation",
	    		JOptionPane.YES_NO_OPTION, 
	    		JOptionPane.QUESTION_MESSAGE, null, null, null);
            
            if (confirm == 0) {
            	
            	MainDriver.save();
               	System.exit(0);
            }
        }
    };
	
	public MainFrame(String caption) {
		
		super(caption);
		initFrame();
	}
	
	private void initFrame() {
		
		LoginScreen loginScreen = new LoginScreen(this);
		loginScreen.initLoginScreen();
		
		// Control over JFrame termination deferred to programmer
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(this.exitListener);
		this.setSize(800, 600);
		// Centers the email client upon execution
		this.setLocationRelativeTo(null); 
		this.setIconImage(ICON.getImage());
		
		configureFileMenu();
		configureContactPanel();
		configureInboxPanel();
	
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPane.addTab("Contacts", null, contactLayout, 
				"Browse contacts and compose emails");
		tabbedPane.addTab("Inbox", null, inboxLayout, 
				"View your emails");
		
		this.add(tabbedPane);
	}
	
	public void populateInboxTable() {
		
		EmailTableModel.data = inboxReader.retrieveMail();
		inboxModel.fireTableDataChanged();
	}
	
	public void updateInboxTable() {
		
		EmailTableModel.data = inboxReader.refreshMail();
		inboxModel.fireTableDataChanged();
	}
	
	public void configureContactPanel() {
		
		contactTable.setVisible(true);
		contactTable.addMouseListener(new EmailTransmissionListener(this, contactTable));		
		final JScrollPane scrollPane = new JScrollPane(contactTable);
		
		// Center the label above the contact pane
		JLabel label = new JLabel("Contacts");
		label.setHorizontalAlignment(SwingConstants.CENTER);
	    contactLayout.add(label, BorderLayout.NORTH); 
	    contactLayout.add(scrollPane, BorderLayout.CENTER);
		
	    JPanel buttons = this.configureTableModButtons();
		contactLayout.add(buttons, BorderLayout.SOUTH);
		contactLayout.setBackground(new Color(255, 101, 1));
	}
	
	public void configureInboxPanel() {
		
		inboxTable.setVisible(true);	
		inboxTable.addMouseListener(new EmailMessageListener(inboxTable));
		final JScrollPane scrollPane = new JScrollPane(inboxTable);
		
		// Center the label above the contact pane
		JLabel label = new JLabel("Inbox");
		label.setHorizontalAlignment(SwingConstants.CENTER);
	    inboxLayout.add(label, BorderLayout.NORTH); 
	    inboxLayout.add(scrollPane, BorderLayout.CENTER);
	    JPanel south = new JPanel(new GridBagLayout());
	    
	    refresh = new JButton("Refresh");
	    refresh.setEnabled(false);
	    retrieve = new JButton("Retrieve mail");
	    retrieve.setHorizontalAlignment(SwingConstants.CENTER);
	    retrieve.setPreferredSize(new Dimension(150, 40));
	    retrieve.setFont(new Font("Serif", Font.BOLD, 16));
	    retrieve.addActionListener(new ActionListener() {
	    	
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		populateInboxTable();
	    		retrieve.setEnabled(false);
	    		refresh.setEnabled(true);
	    	}
	    });
	    GridBagConstraints gbc = new GridBagConstraints();
	    gbc.gridx = 0;
	    south.add(retrieve, gbc);
	    
	    refresh.setHorizontalAlignment(SwingConstants.CENTER);
	    refresh.setPreferredSize(new Dimension(150, 40));
	    refresh.setFont(new Font("Serif", Font.BOLD, 16));
	    refresh.addActionListener(new ActionListener() {
	    	
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		updateInboxTable();
	    	}
	    }); 
	    gbc.gridx = 1;
	    south.add(refresh, gbc);
	    
	    inboxLayout.add(south, BorderLayout.SOUTH);
        inboxLayout.setBackground(new Color(174, 25, 247));
	}
	
	public JPanel configureTableModButtons() {
		
		// Grid layout for add, edit and remove buttons underneath table
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout());
		
		MainFrame.add = new JButton("Add");
		MainFrame.add.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				ContactButton b1 = new AddContactButton(contactTable);
				b1.addListeners();
			}
		});
		
		MainFrame.edit = new JButton("Edit");
		MainFrame.edit.setEnabled(false);
		MainFrame.edit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				ContactButton b2 = new EditContactButton(contactTable);
				b2.addListeners();
			}
		});
		
		MainFrame.remove = new JButton("Delete");
		MainFrame.remove.setEnabled(false);
		MainFrame.remove.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				int confirm = JOptionPane.showOptionDialog(null, 
			    		"Are you sure you want to delete the selected contact(s)?", 
			    		"Deletion Confirmation",
			    		JOptionPane.YES_NO_OPTION, 
			    		JOptionPane.QUESTION_MESSAGE, null, null, null);
				
				if (confirm == 0) {
					
					int rows[] = contactTable.getSelectedRows();
					for (int i = rows.length - 1; i >= 0; --i) {
						
						contactModel.removeObject(rows[i]);
						contactModel.fireTableRowsDeleted(i, i);
					}
				}
			}
		});
	
		buttons.add(add);
		buttons.add(edit);
		buttons.add(remove);
		
		return buttons;
	}
	
	public void configureFileMenu() {
		
		JMenuBar menuBar;
		JMenu fileMenu;
		JMenu configMenu;
		JMenu helpMenu;
		JMenuItem helpMenuItem;
		JMenuItem exitItem;
		
		// Create menu bar
		menuBar = new JMenuBar();
		
		// Create file button 
		fileMenu = new JMenu("File");
		
		// Create close button
		exitItem = new JMenuItem("Exit");
		exitItem.setToolTipText("Close Email Client");
		// Program close button
		exitItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				int confirm = JOptionPane.showOptionDialog(null, 
			    		"Are you sure you want to close the application?", 
			    		"Exit Confirmation",
			    		JOptionPane.YES_NO_OPTION, 
			    		JOptionPane.QUESTION_MESSAGE, null, null, null);
		            
	            if (confirm == 0) {
	            	
	            	MainDriver.save();
	               	dispose();
	            }
			}
		});
		// Add close button to bottom
		fileMenu.add(exitItem);
		
		// Create configuration button
		configMenu = new JMenu("Configuration");
		configMenu.setToolTipText("Configure email client");
		
		JMenuItem configure = new JMenuItem("Configure Client");
		configure.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				new ConfigurationDig(MainFrame.this, inboxModel);
			}
		});
		
		configMenu.add(configure);
		
		// Create help button
		helpMenu = new JMenu("Help");
		helpMenu.setMnemonic(KeyEvent.VK_M);
		// Create about button
		helpMenuItem = new JMenuItem("About");
		helpMenuItem.setToolTipText("About");
		helpMenuItem.setMnemonic(KeyEvent.VK_T);
		helpMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {

				new SystemInformationDig();
			}
		});
		helpMenu.add(helpMenuItem);
		
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		menuBar.add(helpMenu);
		
		this.setJMenuBar(menuBar);
	}
	
	public JButton configureInboxButton() {
		
		final JButton inbox = new JButton("Inbox");
		inbox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		inbox.addMouseListener(new MouseAdapter() {
			
			public void mousePressed(MouseEvent e) {
				
				if (SwingUtilities.isLeftMouseButton(e)) {
					
					remove(contactLayout);
					add(inboxLayout);
					MainFrame.this.getContentPane().validate();
					MainFrame.this.getContentPane().repaint();
				}
			}
		});
		
		return inbox;
	}
	
	public JButton configureContactButton() {
		
		final JButton contacts = new JButton("Contacts");
		contacts.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		contacts.addMouseListener(new MouseAdapter() {
			
			public void mousePressed(MouseEvent e) {
				
				if (SwingUtilities.isLeftMouseButton(e)) {
					
					remove(inboxLayout);
					add(contactLayout);
					MainFrame.this.getContentPane().validate();
					MainFrame.this.getContentPane().repaint();
				}
			}
		});
		
		return contacts;
	}
}
	
	


