package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class AddContactButton extends ContactButton {

	private ContactTable table;
	private ContactTableModel model;
	private JTextField[] textFields;
	
	AddContactButton(ContactTable t) {
		
		super(t);
		table = t;
		model = (ContactTableModel) t.getModel();
		
		// Allows for real time update of new Contact's information
		model.addObject(new Contact("", "", "", ""));
		table.revalidate();
		table.repaint();
		
		super.init();
	}

	public void initializeFrame() {
		
		super.setFrame(new JFrame("Add Contact Information"));
	}
	
	public JTextField[] configureTextFields() {

		textFields = new JTextField[4];
		for (int i = 0; i < 4; ++i) textFields[i] = new JTextField();
		return textFields;
	}
	
	public JButton configureApplyButton() {
	
		JButton apply = new JButton("Apply");
		apply.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				getFrame().setVisible(false);
			}
		});
		
		return apply;
	}

	public JButton configureCancelButton() {
		
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				model.removeObject(table.getRowCount() - 1);
				model.fireTableDataChanged();
				getFrame().setVisible(false);
			}
		});
		
		return cancel;
	}

	public void addListeners() {
		
		super.getFrame().addWindowListener(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
				
				model.removeObject(table.getRowCount() - 1);
				model.fireTableDataChanged();
				getFrame().setVisible(false);
			}
		});
		
		textFields[0].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, model.getRowCount() - 1, 0));
		textFields[1].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, model.getRowCount() - 1, 1));
		textFields[2].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, model.getRowCount() - 1, 2));
		textFields[3].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, model.getRowCount() - 1, 3));
	}
}
