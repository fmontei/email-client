package client;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

public class ContactTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private Vector <Contact> vec;
	private String[] columnNames;
	
	ContactTableModel(String[] cNames, Vector <Contact> data) {
		
		vec = data;
		columnNames = cNames;
	}

    public int getColumnCount() {
    	
        return columnNames.length;
    }

    public int getRowCount() {
    	
        return vec.size();
    }

    public String getColumnName(int col) {
    	
        return columnNames[col];
    }

    public String getValueAt(int row, int col) {
    	
        return vec.get(row).getColumn(col);
    }

    public Class getColumnClass(int c) {
    	
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {

        return false;
    }
    
    public void addObject(Contact p) {
    	
		vec.add(p);
	}
    
    public void removeObject(int row) {
    	
		vec.remove(row);
	}

	public Object getObject(int row) {
		
		return vec.get(row);
	}
	
	public void editObjectData(Object newData, int row, int col) {
    	
    	vec.get(row).editData(newData, col);
    }
	
	public void editAllObjectData(int row, Contact updatedData) {
		
		vec.set(row, updatedData);
	}
}
