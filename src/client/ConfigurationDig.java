package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ConfigurationDig extends JDialog {
		
	private static final long serialVersionUID = 1L;
	private EmailTableModel model;
	private JButton applyButton;
	private JComboBox <String> contactComboBox = new JComboBox <> ();
	private Vector <Configuration> accounts = new Vector <> ();
	private JLabel messageLabel = new JLabel("");
	private boolean cancel = false;
	
	public ConfigurationDig(JFrame parent, EmailTableModel model) {
		
		super(parent, "Configure Email Client", true);
		this.setModalityType(JDialog.ModalityType.DOCUMENT_MODAL);
		this.model = model;
		
		loadContacts();
		initWindow();
	}
	
	public void initWindow() {
		
		this.setSize(new Dimension(400, 300));
		this.setLocationRelativeTo(this.getParent());
		
		JPanel accountPanel = new JPanel(new GridBagLayout());
		JPanel north = new JPanel(new GridLayout(4, 0));
		north.setBorder(BorderFactory.createTitledBorder
				(BorderFactory.createLineBorder
						(new Color(174, 25, 247), 2), "Account Configuration"));
		
		JPanel newContactDialogInfo = new JPanel();
		newContactDialogInfo.add(contactComboBox);
		
		JPanel config = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.SOUTH;
        gbc.weighty = 1;
       
		JPanel buttons = new JPanel(new GridLayout(0, 3));
		JButton add = new JButton("Add Account");
		addButtonListener(add, 
				"New Account Information",
				"Add new account information. " +
				"Must provide username and password.", 
				false);
		JButton edit = new JButton("Edit");
		addButtonListener(edit, 
				"Edit Account Information",
				"Edit preexisting account information.", 
				true);
		JButton remove = new JButton("Remove");
		remove.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				int confirm = JOptionPane.showOptionDialog(null, 
			    		"Are you sure you want to delete the selected contact?", 
			    		"Deletion Confirmation",
			    		JOptionPane.YES_NO_OPTION, 
			    		JOptionPane.QUESTION_MESSAGE, null, null, null);
				
				if (confirm == JOptionPane.YES_OPTION) {
					
					int index = contactComboBox.getSelectedIndex();
					if (index == -1) return;
					contactComboBox.removeItem(contactComboBox.getItemAt(index));
					accounts.remove(index);
					revalidate();
					repaint();
				}
			}
		});
		addScrollOverMessage(remove, "Remove account from drop-down menu.");

		buttons.add(add);
		buttons.add(edit);
		buttons.add(remove);
		config.add(buttons, gbc);
		
		north.add(newContactDialogInfo);
		north.add(Box.createVerticalStrut(20)); 
		messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		messageLabel.setFont(new Font("Serif", Font.ITALIC, 12));
		north.add(messageLabel);
		north.add(config);
		
		JPanel south = new JPanel(new GridBagLayout());
		gbc.weightx = 0.5;
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    
	    cancel = false;
	    JButton connectButton = new JButton("Connect");
	    connectButton.addActionListener(new ActionListener() {
	    	
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		Configuration current = accounts.get(contactComboBox.getSelectedIndex());
	    		
	    		if (LoginScreen.username != current.getEmailAddress()) {
	    			
	    			final JButton yes = new JButton("Yes");
	    			final JButton no  = new JButton("No");
	    			
	    			yes.addActionListener(new ActionListener() {
	    				
	    				public void actionPerformed(ActionEvent e) {
	    					
	    					clearInboxTable();
	    					Window w = SwingUtilities.getWindowAncestor(yes);
	    		    	    if (w != null) 
	    		    	    	w.setVisible(false);
	    				}
	    			});
	    			no.addActionListener(new ActionListener() {
	    				
	    				public void actionPerformed(ActionEvent e) {
	    					
	    					cancel = true;
	    					Window w = SwingUtilities.getWindowAncestor(yes);
	    		    	    if (w != null) 
	    		    	    	w.setVisible(false);
	    				}
	    			});
	    			
	    			final Object[] options = {yes, no};
	    			
	    			JOptionPane.showOptionDialog(
	    				ConfigurationDig.this.getParent(),
    					"Warning: switching email accounts will clear the inbox"
    					+ " of previously retrieved mail.\nDo you want "
    					+ "to continue?",
    				    "Multiple Account Login Warning",
    				    JOptionPane.YES_NO_CANCEL_OPTION,
    				    JOptionPane.WARNING_MESSAGE,
    				    null,
    				    options,
    				    options[0]);
	    		}
	    		
	    		if (cancel) return;
	    		
	    		LoginScreen.username = current.getEmailAddress();
	    		LoginScreen.password = current.getPassword();
	    		LoginScreen.validateCredentials();
	    	}
	    });
	    addScrollOverMessage(connectButton, 
	    		"Connect to the selected email " +
	    		"account from the drop-down menu.");
	    south.add(connectButton, gbc);
	    
	    gbc.weightx = 0.5;
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.gridx = 1;
	    gbc.gridy = 0;
	    JButton saveContactButton = new JButton("Save");
	    saveContactButton.addActionListener(new ActionListener() {
	    	
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		DataStore.config = accounts;
	    		setVisible(false);
	    	}
	    });
	    addScrollOverMessage(saveContactButton, "Save changes.");
	    south.add(saveContactButton, gbc);
	    
		gbc.weightx = 0.5;
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.gridx = 2;
	    gbc.gridy = 0;
	    JButton cancelContactButton = new JButton("Cancel");
	    cancelContactButton.addActionListener(new ActionListener() {
	    	
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		setVisible(false);
	    	}
	    });
	    addScrollOverMessage(cancelContactButton, 
	    		"Changes will not be saved after program is closed.");
	    south.add(cancelContactButton, gbc);
	
	    gbc.weighty = 2.5;
	    gbc.fill = GridBagConstraints.VERTICAL;
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    gbc.ipadx = 100;
	    accountPanel.add(north, gbc);
	    gbc.weighty = 0.5;
	    gbc.fill = GridBagConstraints.VERTICAL;
	    gbc.gridx = 0;
	    gbc.gridy = 10;
	    accountPanel.add(south, gbc);
	    JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Configure Email Accounts", null,
                          accountPanel,
                          "Add/edit/remove account information");
	    
		this.add(tabbedPane);
		this.setVisible(true);
	}
	
	public void loadContacts() {
		
		try {
			for (Configuration c : DataStore.config) {
				
				accounts.add(c);
				contactComboBox.addItem(c.getEmailAddress());
			}
		} catch(Exception e) {}
	}
	
	public void addButtonListener(JButton button, final String borderLabel, 
			final String message, final boolean editable) {
		
		button.addMouseListener(new MouseAdapter() {
		
			public void mousePressed(MouseEvent e) {
				
				final JDialog newContactDialog = new JDialog();
				newContactDialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
				newContactDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
				
				JPanel newContactPanel = new JPanel(new GridLayout(4, 2));
				newContactPanel.setBorder(BorderFactory.createTitledBorder
						(BorderFactory.createLineBorder
								(new Color(25, 1, 235), 2), 
						    	borderLabel));
				
				JLabel email = new JLabel("Email:");
				email.setHorizontalAlignment(SwingConstants.CENTER);
				
				int index; 
				String originalName, originalPassword;
				if (contactComboBox.getSelectedIndex() == -1) {
					
					index = 0;
					originalPassword = "null";
					originalName = "null";
				}
				
				else {
					
					index = contactComboBox.getSelectedIndex();
					originalPassword = accounts.get(index).getPassword();
					originalName = (String) contactComboBox.getSelectedItem();
				}
		
				final JTextField nameTextField = (editable) ? new JTextField(originalName) : new JTextField();
				nameTextField.getDocument().addDocumentListener(new DocumentListener() {
	
					public void changedUpdate(DocumentEvent arg0) {
						
						if (editable) {
							
							applyButton.setEnabled(true);
							revalidate();
							repaint();
						}
					}
					public void insertUpdate(DocumentEvent arg0) {
						
						if (!nameTextField.getText().equals("")) {
							
							applyButton.setEnabled(true);
							revalidate();
							repaint();
						}
					}
					public void removeUpdate(DocumentEvent arg0) {
						
						if (nameTextField.getText().equals("")) {
							
							applyButton.setEnabled(false);
							revalidate();
							repaint();
						}
					}
				});
				
				final JTextField passwordTextField = (editable) ? new JTextField(originalPassword) : new JTextField();
				passwordTextField.getDocument().addDocumentListener(new DocumentListener() {
	
					public void changedUpdate(DocumentEvent arg0) {
						
						if (editable) {
							
							applyButton.setEnabled(true);
							revalidate();
							repaint();
						}
					}
					public void insertUpdate(DocumentEvent arg0) {
						
						if (!passwordTextField.getText().equals("")) {
							
							applyButton.setEnabled(true);
							revalidate();
							repaint();
						}
					}
					public void removeUpdate(DocumentEvent arg0) {
						
						if (passwordTextField.getText().equals("")) {
							
							applyButton.setEnabled(false);
							revalidate();
							repaint();
						}
					}
				});
				
				nameTextField.setPreferredSize(new Dimension(20, 20));
				JLabel password = new JLabel("Password:");
				password.setHorizontalAlignment(SwingConstants.CENTER);
				passwordTextField.setPreferredSize(new Dimension(20, 20));
				JLabel password2 = new JLabel("Confirm Password:");
				password2.setHorizontalAlignment(SwingConstants.CENTER);
				final JTextField passwordTextField2 = new JTextField();
				passwordTextField.setPreferredSize(new Dimension(20, 20));
				
				applyButton = new JButton("Apply");
				applyButton.setEnabled(false);
				applyButton.addActionListener(new ActionListener() {
	
					public void actionPerformed(ActionEvent e) {
	
						if (!passwordTextField.getText().equals(passwordTextField2.getText())) {
							
							JOptionPane.showMessageDialog(
							        newContactDialog, 
							        "Error: Passwords do not match", 
							        "Password Error", 
							        JOptionPane.ERROR_MESSAGE);
							
									return;
						}
						
						if (editable) {
							
							final int index = contactComboBox.getSelectedIndex();
							final String text1 = nameTextField.getText();
							final String text2 = passwordTextField.getText();
							contactComboBox.setSelectedItem(text1);
							accounts.set(index, new Configuration(text1, text2));
						}
						
						else {
							
							contactComboBox.addItem(nameTextField.getText());
							accounts.add(new Configuration(nameTextField.getText(),
									passwordTextField.getText()));
						}
						
						revalidate();
						repaint();
						newContactDialog.setVisible(false);
					}
				});
				
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						
						newContactDialog.setVisible(false);
					}
				});
				
				newContactPanel.add(email);
				newContactPanel.add(nameTextField);
				newContactPanel.add(password);
				newContactPanel.add(passwordTextField);
				newContactPanel.add(password2);
				newContactPanel.add(passwordTextField2);
				newContactPanel.add(applyButton);
				newContactPanel.add(cancelButton);
				newContactDialog.add(newContactPanel);
				
				newContactDialog.setSize(new Dimension(330, 140));
				newContactDialog.setLocationRelativeTo(ConfigurationDig.this.getParent());
				newContactDialog.setVisible(true);
			}
			
			public void mouseEntered(MouseEvent e) {
				
				messageLabel.setText(message);
				messageLabel.setVisible(true);
			}
			
			public void mouseExited(MouseEvent e) {
				
				messageLabel.setVisible(false);
			}
		});
	}
	
	public void addScrollOverMessage(JButton button, final String message) {
		
		button.addMouseListener(new MouseAdapter() {
			
			public void mouseEntered(MouseEvent e) {
				
				messageLabel.setText(message);
				messageLabel.setVisible(true);
			}
			
			public void mouseExited(MouseEvent e) {
				
				messageLabel.setVisible(false);
			}
		});
	}
	
	public void clearInboxTable() {
		
		EmailTableModel.data.clear();
		InboxReader.messageCount = 1;
		model.fireTableDataChanged();
		MainFrame.refresh.setEnabled(false);
		MainFrame.retrieve.setEnabled(true);
	}
}
