package client;

import java.awt.Color;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class ContactDocumentListener implements DocumentListener {

	private String data = new String("");
	private ContactTable table;
	private ContactTableModel model;
	private int col;
	private int row;
	
	public ContactDocumentListener(
			ContactTable table, 
			ContactTableModel model, 
			int row, 
			int col) {
		
		this.table = table;
		this.model = model;
		this.row = row;
		this.col = col;
	}
	
	private void liveUpdate(DocumentEvent e) {
		
		Document doc = e.getDocument();
		try {
			data = doc.getText(0, doc.getLength());
		} catch (BadLocationException ex) {

		}

		model.editObjectData(data, row, col);
		table.repaint();
		table.revalidate();
	}
	
	public void changedUpdate(DocumentEvent e) {
			
	}

	public void insertUpdate(DocumentEvent e) {
	
		liveUpdate(e);
	}

	public void removeUpdate(DocumentEvent e) {
		
		liveUpdate(e);
	}
	
	public String getData() {
		
		return data;
	}
}
