package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class EmailTransmissionDig extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private String recipients = "";
	private JTextField[] fields = new JTextField[4];
	JTable table;
	ContactTableModel model;
	
	EmailTransmissionDig(JFrame f, JTable t) {
		
		super(f, "Send Email", true);
		this.setModalityType(JDialog.ModalityType.DOCUMENT_MODAL);
		table = t;
		model = (ContactTableModel) table.getModel();
		
		this.setSize(500, 500);
		this.setLocationRelativeTo(f);
		this.setLayout(new BorderLayout());

		for (int i = 0; i < 4; ++i)
			fields[i] = new JTextField();
		JLabel to = new JLabel("To  ");
		JLabel CC = new JLabel("CC  ");
		JLabel BC = new JLabel("BC  ");
		JLabel subject = new JLabel("Subject  ");
		
		final JPanel top = new JPanel();
	    top.setBorder(BorderFactory.createTitledBorder("Recipients"));
	    BoxLayout layout = new BoxLayout(top, BoxLayout.Y_AXIS);
	    top.setLayout(layout);
	    
	    int[] rows = table.getSelectedRows();
	    for (int i = 0; i < rows.length; ++i) {
	    	
	    	String email = ((Contact) model.getObject(rows[i])).getEmailAddress();
	    	if (!Validation.isValid.matcher(email).matches()) continue;
	    	recipients += email + ", ";
	    }
	    
	    if (recipients.endsWith(", ")) 
	    	recipients = recipients.substring(0, recipients.length() - 2);
	    
	    fields[0].setText(recipients);
	    addNewRow(top, to,      fields[0]);
	    addNewRow(top, CC,      fields[1]);
	    addNewRow(top, BC,      fields[2]);
	    top.add(Box.createVerticalStrut(5));
	    addNewRow(top, subject, fields[3]);
		
		final JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		
		JScrollPane textPane = new JScrollPane(textArea);
		textPane.setPreferredSize(new Dimension(500, 300));
		textPane.setHorizontalScrollBarPolicy
			(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		textPane.setVerticalScrollBarPolicy
			(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		final JLabel failed = new JLabel("Email send failure");
		final JLabel success = new JLabel("Email successfully sent");
		failed.setVisible(false);
		success.setVisible(false);
		failed.setForeground(Color.RED);
	    success.setForeground(Color.GREEN);
		
		JButton sendButton = new JButton("Send");
		sendButton.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent event) {
		
				if (SwingUtilities.isLeftMouseButton(event)) {
					
					success.setVisible(false);
					failed.setVisible(false);			
					try {
						if (!LoginScreen.validateCredentials()) {
		    			
			    			JOptionPane.showMessageDialog(
			    					EmailTransmissionDig.this.getParent(),
			    					"Login Failed: "
			    					+ "Select another email account\n"
			    					+ " or manually change password settings"
			    					+ "under configuration.",
			    				    "Login Error",
			    				    JOptionPane.ERROR_MESSAGE);
						}
						
						Message msg = new MimeMessage(LoginScreen.loginSession);
						
						String[] tempRecipients = recipients.split(",");
						InternetAddress[] address = 
								new InternetAddress[tempRecipients.length];
						for (int i = 0; i < tempRecipients.length; ++i) {
							address[i] = new InternetAddress(tempRecipients[i]);
						}
						
						msg.addRecipients(RecipientType.TO, address);
						msg.setSubject(fields[3].getText());
						msg.setFrom(new InternetAddress(LoginScreen.username));
						msg.setText(textArea.getText());
						Transport.send(msg);
						
						success.setVisible(true);
					} catch (Exception e) {
						failed.setVisible(true);
					}
				}	
			}
		});
		
		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		
		final JPanel bottom = new JPanel();
	    bottom.setBorder(BorderFactory.createTitledBorder("Send Email"));
	    bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
	    bottom.setBorder(BorderFactory.createRaisedBevelBorder());
	    bottom.add(sendButton);
	    bottom.add(Box.createHorizontalStrut(100));
	    bottom.add(success);
	    bottom.add(failed);
	    bottom.add(Box.createHorizontalGlue());
	    bottom.add(closeButton);
	    
	    this.add(top, BorderLayout.NORTH);
		this.add(textPane, BorderLayout.CENTER);
		this.add(bottom, BorderLayout.SOUTH);
		this.setVisible(true);
	}
	
	public void addNewRow(JPanel top, JLabel label, JTextField field) {
		
		field.setPreferredSize(new Dimension(20, 20));
		JPanel row = new JPanel();
	    BoxLayout horizontal = new BoxLayout(row, BoxLayout.X_AXIS);
	    row.setLayout(horizontal);
	    label.setPreferredSize(new Dimension(50, 10));
	    row.add(label);
	    row.add(new JSeparator(SwingConstants.VERTICAL));
	    row.add(Box.createHorizontalStrut(5));
	    row.add(field);
	    top.add(row);
	}
}
