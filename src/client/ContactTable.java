package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

public class ContactTable extends JTable {

	public ContactTable(String caption, ContactTableModel model) {
		
		super(model);
		this.setSize(new Dimension(800,600));
		// Selects a particular cell, rather than the whole row at a time
		this.setCellSelectionEnabled(true);
		
	    ListSelectionModel rowSelectionModel = this.getSelectionModel();
	 
	    rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
	    	
		    public void valueChanged(ListSelectionEvent e) {
	
		    	Boolean selected = false;
		        for (int i = 0; i < getRowCount(); i++) {
		
		            if (isRowSelected(i)) selected = true;	          
		        }
		        
		        if (!selected) { 
		        	
		        	MainFrame.edit.setEnabled(false); 
		        	MainFrame.remove.setEnabled(false); 
	        	}
		        
		        else {
		        	
		        	MainFrame.edit.setEnabled(true); 
		        	MainFrame.remove.setEnabled(true); 
	        	}
	      	}
	    });
	}
	
	public Point getPopupLocation(MouseEvent event) {
        
        ((JComponent) event.getComponent()).putClientProperty(
        		"table.popupLocation", event != null ? event.getPoint() : null);
        return super.getPopupLocation(event);
    }
}
