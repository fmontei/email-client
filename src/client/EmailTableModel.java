package client;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;

public class EmailTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	protected static Vector <iMessage> data;
	private String[] columnNames;
	
	public EmailTableModel(String[] columnNames, Vector <iMessage> data) {
		
		this.data = data;
		this.columnNames = columnNames;
	}

    public int getColumnCount() {
    	
        return columnNames.length;
    }

    public int getRowCount() {
    	
        return data.size();
    }

    public String getColumnName(int col) {
    	
        return columnNames[col];
    }

    public String getValueAt(int row, int col) {
    	
        return data.get(row).getColumn(col);
    }

    public boolean isCellEditable(int row, int col) {

        return false;
    }
    
    public void addObject(iMessage m) {
    	
    	data.add(m);
	}
    
    public void removeObject(int row) {
    	
		data.remove(row);
	}

	public Object getObject(int row) {
		
		return data.get(row);
	}
	
	public void editObjectData(Object newData, int row, int col) {
    	
    	data.get(row).editData(newData, col);
    }
	
	public void editAllObjectData(int row, iMessage updatedData) {
		
		data.set(row, updatedData);
	}
}
