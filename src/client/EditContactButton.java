package client;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class EditContactButton extends ContactButton {

	private ContactTable table;
	private ContactTableModel model;
	private JTextField[] textFields;
	private String nameText;
	private String pAddressText;
	private String eAddressText;
	private String phoneText;
	private Color  color;
	
	EditContactButton(ContactTable t) {
		
		super(t);
		table = t;
		model = (ContactTableModel) table.getModel();
		super.init();
	}

	public void initializeFrame() {
		
		super.setFrame(new JFrame("Edit Contact Information"));
	}
	
	public JTextField[] configureTextFields() {
		
		nameText = ((Contact) model.getObject(table.getSelectedRow())).getName();
		pAddressText = ((Contact) model.getObject(table.getSelectedRow())).getPostalAddress();
		eAddressText = ((Contact) model.getObject(table.getSelectedRow())).getEmailAddress();
		phoneText = ((Contact) model.getObject(table.getSelectedRow())).getPhoneNumber();
		
		textFields = new JTextField[4];
		textFields[0] = new JTextField(nameText);
		textFields[1] = new JTextField(eAddressText);
		textFields[2] = new JTextField(pAddressText);
		textFields[3] = new JTextField(phoneText);
		
		return textFields;
	}
	
	public JButton configureApplyButton() {
	
		JButton apply = new JButton("Apply");
		apply.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				getFrame().setVisible(false);
			}
		});
		
		return apply;
	}

	public JButton configureCancelButton() {
		
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				model.editAllObjectData(table.getSelectedRow(), new Contact(
																nameText, 
																eAddressText, 
																pAddressText, 
																phoneText));

				model.fireTableDataChanged();
				getFrame().setVisible(false);
			}
		});
		
		return cancel;
	}

	public void addListeners() {
		
		super.getFrame().addWindowListener(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
				
				model.editAllObjectData(table.getSelectedRow(), new Contact(
																nameText, 
																eAddressText,
																pAddressText,
																phoneText));
				model.fireTableDataChanged();
				getFrame().setVisible(false);
			}
		});
		
		textFields[0].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, table.getSelectedRow(), 0));
		textFields[1].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, table.getSelectedRow(), 1));
		textFields[2].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, table.getSelectedRow(), 2));
		textFields[3].getDocument().addDocumentListener
			(new ContactDocumentListener(table, model, table.getSelectedRow(), 3));
	}
}
