package client;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class EmailTable extends JTable {

	private static final long serialVersionUID = 1L;

	public EmailTable(String caption, AbstractTableModel model) {
		
		super(model);
		this.setSize(new Dimension(800,600));
		this.setCellSelectionEnabled(true);
	}
}
