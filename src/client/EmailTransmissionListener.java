package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class EmailTransmissionListener implements MouseListener {
	
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JTable table;
	
	public EmailTransmissionListener(JFrame f, JTable t) {
		
		frame = f;
		table = t;
	}
	
	public void mousePressed(MouseEvent e) {
		
		final JPopupMenu popup;
		
		//MainFrame.edit.setEnabled(true);
		//MainFrame.remove.setEnabled(true);
		
		// If the left button is double-clicked
		if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
	    	     
			new EmailTransmissionDig(frame, table);
		}
		
		// If the right button is clicked
		else if (SwingUtilities.isRightMouseButton(e)) {
			
			JTable target = (JTable) e.getSource();
			int row = target.getSelectedRow();
			int column = target.getSelectedColumn();
			
			popup = new JPopupMenu("Contact Popup");
			popup.setLocation(row, column);
			JMenuItem openMenuItem = new JMenuItem("Compose");
			openMenuItem.addMouseListener(new MouseAdapter() {
				
				public void mousePressed(MouseEvent e) {
					
					if (SwingUtilities.isLeftMouseButton(e)) {
						
						new EmailTransmissionDig(frame, table);
					}
				}
			});
		
			AbstractAction copyMenuItem = new AbstractAction("Copy") {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
			
			       Point p = (Point) table.getClientProperty
			    		   ("table.popupLocation");
			       if (p != null) { 
			    	  
			           int r = table.rowAtPoint(p);
			           int c = table.columnAtPoint(p);

			           StringSelection entry = new StringSelection
			           (table.getValueAt(table.convertRowIndexToView(r), 
			                   table.convertRowIndexToView(c)).toString());
			           Clipboard clipboard = 
			        		   Toolkit.getDefaultToolkit().getSystemClipboard();
			           clipboard.setContents(entry, entry); 
			       } else {
			        
			    	   // Handle exception when copy fails?
			       }
			    }
			};
				
			popup.add(openMenuItem);
			popup.add(copyMenuItem);
			
			popup.setLocation(e.getLocationOnScreen());
			popup.setVisible(true);
			// Creates "one instance" of the pop-up menu
			table.setComponentPopupMenu(popup);
		}
	}
	
	public void mouseClicked(MouseEvent e) { 
	}

	public void mouseEntered(MouseEvent e) {		
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}
}

