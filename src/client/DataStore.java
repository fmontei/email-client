package client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

public class DataStore implements Storable {
	
	protected static Vector <Configuration> config = new Vector <> ();
	protected static Vector <Contact> contacts = new Vector <> ();
	
	private static DataStore instance = null;
	
	private static final File configSer = new File(System.getProperty("user.dir") + "\\Configuration.ser");
	private static final File contactSer = new File(System.getProperty("user.dir") + "\\Contact.ser");
	
	private DataStore() {
		
	}
	
	public void setConfig(Vector <Configuration> config) {
		
		this.config = config;
	}
	
	public void setContact(Contact contact) {
		
		contacts.add(contact);
	}
	
	public void setContacts(Vector <Contact> contacts) {
		
		this.contacts = contacts;
	}
	
	public Vector <Configuration> getConfig() {
		
		return config;
	}
	
	public Vector <Contact> getContacts() {
		
		return contacts;
	}
	
	public void store() throws IOException {	

		FileOutputStream fileOut1 = new FileOutputStream(configSer);
		FileOutputStream fileOut2 = new FileOutputStream(contactSer);
		ObjectOutputStream out1 = new ObjectOutputStream(fileOut1);
		ObjectOutputStream out2 = new ObjectOutputStream(fileOut2);
		out1.writeObject(config);
		out2.writeObject(contacts);
		out1.close();
		out2.close();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieve() throws IOException, ClassNotFoundException {
		
		if (!configSer.exists())
			configSer.createNewFile();
		if (!contactSer.exists())
			contactSer.createNewFile();
		FileInputStream fileIn1 = new FileInputStream(configSer);
		FileInputStream fileIn2 = new FileInputStream(contactSer);
		ObjectInputStream in1 = new ObjectInputStream(fileIn1);
		ObjectInputStream in2 = new ObjectInputStream(fileIn2);
		config = (Vector <Configuration>) in1.readObject();
		contacts = (Vector <Contact>) in2.readObject();
		in1.close();
		in2.close();
	}
	
	protected static DataStore getInstance() {
		
		if (instance == null)
			instance = new DataStore();
		
		return instance;
	}
}
