package client;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class EmailMessageListener extends MouseAdapter {

	EmailTable table;
	EmailTableModel model;
	
	public EmailMessageListener(EmailTable table) {
		
		this.table = table;
		model = (EmailTableModel) table.getModel();
	}
	
	public void mousePressed(MouseEvent e) {
		
		if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
			
			final String content = ((iMessage) model.getObject(table.getSelectedRow())).getContent();
			String title = "Reading message from ";
			String subject = model.getValueAt(table.getSelectedRow(), 0);
			if (subject.contains("<"))
				subject = subject.substring(0, subject.indexOf("<"));
			title += subject;
			
			final JDialog messageViewer = new JDialog();
			messageViewer.setLayout(new GridBagLayout());
			messageViewer.setTitle(title);
			messageViewer.setResizable(true);
			messageViewer.setModalityType(Dialog.ModalityType.DOCUMENT_MODAL);
			
			JTextArea textBox = new JTextArea();
			textBox.setText(content);
			textBox.setCaretPosition(0);
			textBox.setEditable(false);
			textBox.setLineWrap(true);
			textBox.setWrapStyleWord(true);
			
			JScrollPane pane = new JScrollPane(textBox);
			pane.setPreferredSize(new Dimension(400, 300));
		
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridy = 0;
			JButton ok = new JButton("Ok");
			ok.setPreferredSize(new Dimension(50, 20));
			ok.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					
					messageViewer.dispose();
				}
			});
			
			messageViewer.add(pane, gbc);
			gbc.gridy = 1;
			messageViewer.add(ok, gbc);
			messageViewer.pack();
			messageViewer.setLocationRelativeTo(messageViewer.getParent());
			messageViewer.setVisible(true);
		}
	}
}
