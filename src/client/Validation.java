package client;

import java.util.regex.Pattern;

public class Validation {

	protected static final Pattern isValid = Pattern.compile(
	          "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%" +
	          "&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])" +
	          "?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
}
